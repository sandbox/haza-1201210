<?php

/**
 * Field handler to present customer profile edit link not on administration
 * pages.
 */
class commerce_addressbook_views_handler_field_address_operations_edit extends commerce_customer_handler_field_customer_profile_link {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['profile_id'] = 'profile_id';
  }

  function render($values) {

    $profile = commerce_customer_profile_new();
    $profile->profile_id = $this->get_value($values, 'profile_id');
    $profile->type = $this->get_value($values, 'type');
    $profile->uid = $this->get_value($values, 'uid');

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');

    return l($text, 'address/edit/' . $profile->profile_id, array('query' => drupal_get_destination()));
  }
}

/**
 * Field handler to present customer profile delete link not on administration
 * pages.
 */
class commerce_addressbook_views_handler_field_address_operations_delete extends commerce_customer_handler_field_customer_profile_link {
  function construct() {
    parent::construct();

    $this->additional_fields['type'] = 'type';
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['profile_id'] = 'profile_id';
  }

  function render($values) {

    $profile = commerce_customer_profile_new();
    $profile->profile_id = $this->get_value($values, 'profile_id');
    $profile->type = $this->get_value($values, 'type');
    $profile->uid = $this->get_value($values, 'uid');

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');

    return l($text, 'address/delete/' . $profile->profile_id, array('query' => drupal_get_destination()));
  }
}
