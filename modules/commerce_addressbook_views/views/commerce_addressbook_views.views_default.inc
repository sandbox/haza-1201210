<?php

/**
 * Views for the shopping cart.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_addressbook_views_views_default_views() {

  $view = new view;
  $view->name = 'addressbook_manage_addresses';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_customer_profile';
  $view->human_name = 'Manage addresses';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Address List';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['grouping'] = 'type';
  $handler->display->display_options['style_options']['fill_single_line'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Commerce Customer Profile: Owner */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'commerce_customer_profile';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = 0;
  /* Field: Commerce Customer profile: Address */
  $handler->display->display_options['fields']['commerce_customer_address']['id'] = 'commerce_customer_address';
  $handler->display->display_options['fields']['commerce_customer_address']['table'] = 'field_data_commerce_customer_address';
  $handler->display->display_options['fields']['commerce_customer_address']['field'] = 'commerce_customer_address';
  $handler->display->display_options['fields']['commerce_customer_address']['label'] = '';
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['commerce_customer_address']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_customer_address']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['commerce_customer_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['commerce_customer_address']['field_api_classes'] = 0;
  /* Field: Commerce Customer Profile: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'commerce_customer_profile';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['type']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['type']['link_to_profile'] = 0;
  /* Field: Commerce Customer Profile: Profile ID */
  $handler->display->display_options['fields']['profile_id']['id'] = 'profile_id';
  $handler->display->display_options['fields']['profile_id']['table'] = 'commerce_customer_profile';
  $handler->display->display_options['fields']['profile_id']['field'] = 'profile_id';
  $handler->display->display_options['fields']['profile_id']['label'] = '';
  $handler->display->display_options['fields']['profile_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['profile_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['profile_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['profile_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['profile_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['profile_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['profile_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['profile_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['profile_id']['hide_alter_empty'] = 0;
  /* Field: Commerce Customer Profile: User edit link */
  $handler->display->display_options['fields']['user_edit_link']['id'] = 'user_edit_link';
  $handler->display->display_options['fields']['user_edit_link']['table'] = 'commerce_customer_profile';
  $handler->display->display_options['fields']['user_edit_link']['field'] = 'user_edit_link';
  $handler->display->display_options['fields']['user_edit_link']['label'] = '';
  $handler->display->display_options['fields']['user_edit_link']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['external'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['user_edit_link']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['user_edit_link']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['alter']['html'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['user_edit_link']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['user_edit_link']['hide_empty'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['empty_zero'] = 0;
  $handler->display->display_options['fields']['user_edit_link']['hide_alter_empty'] = 0;
  /* Field: Commerce Customer Profile: User delete link */
  $handler->display->display_options['fields']['user_delete_link']['id'] = 'user_delete_link';
  $handler->display->display_options['fields']['user_delete_link']['table'] = 'commerce_customer_profile';
  $handler->display->display_options['fields']['user_delete_link']['field'] = 'user_delete_link';
  $handler->display->display_options['fields']['user_delete_link']['label'] = '';
  $handler->display->display_options['fields']['user_delete_link']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['external'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['user_delete_link']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['user_delete_link']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['alter']['html'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['user_delete_link']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['user_delete_link']['hide_empty'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['empty_zero'] = 0;
  $handler->display->display_options['fields']['user_delete_link']['hide_alter_empty'] = 0;
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['uid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['uid']['not'] = 0;
  /* Filter criterion: Commerce Customer Profile: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'commerce_customer_profile';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Commerce Customer Profile: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'commerce_customer_profile';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'shipping' => 'shipping',
    'billing' => 'billing',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'user/%/manage-addresses';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Manage Addresses';
  $handler->display->display_options['menu']['weight'] = '0';


  $views[$view->name] = $view;

  return $views;
}
