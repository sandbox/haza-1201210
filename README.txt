This module provides the ability to have multiple addresses (shipping and billing) on the same customer profile and to use them on the checkout form for Drupal Commerce.

Features:
* Define an admin UI for the user to manage addresses.
* Allow the user to save multiple addresses for billing and shipping.
* Expose a selector in the "billing information" pane with all the addresses of the current logged in customer.
* � Same for "shipping information"

Todo:
* Add checkbox for copying the billing addresses into shipping addresses.
