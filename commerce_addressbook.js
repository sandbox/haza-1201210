Drupal.behaviors.commerce_addressbook = {
  attach: function(context){
    jQuery('[name*="collected_addresses"]').once("commerce_address-processed").change(function(){
      var obj;
      obj = jQuery.parseJSON(Drupal.settings.commerce_addressbook);
      console.log(obj);
      var selectedVal = jQuery(this).val();
      if(selectedVal != 'none'){
      // determines if the country has changed and triggers the onchange event before filling in the fields so that the right fields get changed
        if(obj[selectedVal]['country'] != jQuery('[name*="country"]').val()){
          jQuery('[name*="country"]').val(obj[selectedVal]['country']);
          jQuery('[name*="country"]').trigger('change');
        }
        for(name in obj[selectedVal]){
          var val = obj[selectedVal][name];
          if(name !== 'undefined'){
            jQuery('[name*="'+name+ '"]').val(val);
          }
        }
      }
    })
  }
};