<?php

/**
 * Ensures the address field is present on the specified customer profile bundle.
 */
function commerce_addressbook_configure_default_checkbox_field($profile_type) {
  // Look for or add an address field to the customer profile type.
  $field_name = 'default_address';
  $field = field_info_field($field_name);
  $instance = field_info_instance('commerce_customer_profile', $field_name, $profile_type);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'list_boolean',
      'cardinality' => 1,
      'entity_types' => array('commerce_customer_profile'),
      'translatable' => FALSE,
    );

    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => 'commerce_customer_profile',
      'bundle' => $profile_type,
      'label' => t('Default address'),
      'required' => FALSE,
      'widget' => array(
        'type' => 'options_onoff',
        'weight' => 10,
        'settings' => array(
          'display_label' => TRUE,
        )
      ),
      'settings' => array(),
      'display' => array(),
    );

    // Set the default display formatters for various view modes.
    foreach (array('default', 'customer', 'administrator') as $view_mode) {
      $instance['display'][$view_mode] = array(
        'label' => 'hidden',
        'weight' => -10,
      );
    }

    field_create_instance($instance);
  }

}

