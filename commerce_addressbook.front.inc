<?php

/**
 * Callback to create an entity of author entity type.
 */
function commerce_addressbook_add_address($type = null, $op = 'add', $id = NULL) {
  if ($op == 'add') {
    drupal_set_title('Add ' . $type . ' address');
    $profile = entity_create('commerce_customer_profile', array('type' => $type));
    return drupal_get_form('commerce_addressbook_add_form', $profile);
  }
  else if ($op == 'edit' && $id != NULL) {
    $profile = entity_load_single('commerce_customer_profile', $id);
    isset($profile->type) ? drupal_set_title('Edit ' . $profile->type . ' address') : drupal_set_title('Edit address')  ;
    return drupal_get_form('commerce_addressbook_add_form', $profile, $id);
  }
}

/**
 * Entity creation form.
 */
function commerce_addressbook_add_form($form, &$form_state, $profile) {
 // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_customer') . '/includes/commerce_customer_profile.forms.inc';

  // Ensure the owner name is accessible if the uid is set.
  if (!empty($profile->uid) && $owner = user_load($profile->uid)) {
    $profile->name = $owner->name;
  }

  if (empty($profile->created)) {
    $profile->date = format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s O');
  }

  // Add the field related form elements.
  $form_state['customer_profile'] = $profile;
  field_attach_form('commerce_customer_profile', $profile, $form, $form_state);


  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 100,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save profile'),
    '#submit' => $submit + array('commerce_addressbook_profile_form_submit'),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'commerce_addressbook_profile_form_validate';

  return $form;
}



/**
 * Validation callback for commerce_customer_profile_form().
 */
function commerce_addressbook_profile_form_validate($form, &$form_state) {
  global $user;
  $profile = $form_state['customer_profile'];

  // Validate the "owned by" field.
  $form_state['values']['name'] = $user->name;

  // Notify field widgets to validate their data.
  field_attach_form_validate('commerce_customer_profile', $profile, $form, $form_state);
}

/**
 * Submit callback for commerce_customer_profile_form().
 */
function commerce_addressbook_profile_form_submit($form, &$form_state) {
  global $user;

  $profile = &$form_state['customer_profile'];

  // status should be 1 by default.
  $profile->status = 1;

  // Set the profile's owner uid based on the supplied name.
  if (!empty($form_state['values']['name']) && $account = user_load_by_name($form_state['values']['name'])) {
    $profile->uid = $account->uid;
  }
  else {
    $profile->uid = 0;
  }

  // Notify field widgets.
  field_attach_submit('commerce_customer_profile', $profile, $form, $form_state);

  // Save the profile.
  commerce_customer_profile_save($profile);

  // Redirect based on the button clicked.
  drupal_set_message(t('Profile saved.'));
}


/**
 * Form callback wrapper: confirmation form for deleting a customer profile.
 *
 * @param $profile
 *   The customer profile object being deleted by this form.
 *
 * @see commerce_customer_customer_profile_delete_form()
 */
function commerce_addressbook_delete_address($profile) {
  return drupal_get_form('commerce_addressbook_delete_address_form', $profile);
}


/**
 * Form callback: confirmation form for deleting a profile.
 *
 * @param $profile
 *   The profile object to be deleted.
 *
 * @see confirm_form()
 */
function commerce_addressbook_delete_address_form($form, &$form_state, $profile) {
  $form_state['customer_profile'] = $profile;

  $form['#submit'][] = 'commerce_addressbook_customer_profile_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete this profile?'),
    '',
    '<p>' . t('Deleting this profile cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_customer_profile_delete_form().
 */
function commerce_addressbook_customer_profile_delete_form_submit($form, &$form_state) {
  $profile = $form_state['customer_profile'];

  if (commerce_customer_profile_delete($profile->profile_id)) {
    drupal_set_message(t('The profile has been deleted.'));
    watchdog('commerce_customer_profile', 'Deleted customer profile @profile_id.', array('@profile_id' => $profile->profile_id), WATCHDOG_NOTICE);
    $form_state['redirect'][] = 'user';
  }
//  else {
//    drupal_set_message(t('The profile could not be deleted.'), 'error');
//  }
}

